import log from '@ajar/marker'; 
import http from 'http';

// const PORT = process.env.PORT;
// const HOST = process.env.HOST;
const { PORT, HOST } = process.env; // destructuring 


//create an http web server
const server = http.createServer((req, res) => {
    // מידע על הלקוח מי שמבקש את המידע
    // מספק תשובה ללקוח
    res.statusCode = 200;
    res.setHeader('agenda', 'political');
    res.setHeader('anything', 'goes');
    res.setHeader('some-single-header', 'some-single-header');

    // http://localhost:3030/users/lister?month=April&temp=32
    // req.headers.host --> localhost:3030
    // req.url --> /users/lister?month=April&temp=32

    let obj = {
        href: 'http://' + req.headers.host + req.url,
        url: req.url,
        method: req.method,
        host: req.headers.host,
        protocol: 'http',
        httpVersion: req.httpVersion,
        pathName: new URL('http' + '://' + req.headers.host + req.url).pathname,
        querystring: req.url.slice(req.url.indexOf('?') + 1).split('&').reduce((pre, cur) => {
            const keyval = cur.split('=');
            pre[keyval[0]] = keyval[1];
            return pre;
        }, {}),
        "user-agent": req.headers['user-agent'],
        connection: req.headers.connection
    }
    res.end(JSON.stringify(obj));

    log.magenta(req.method,obj);
});

//have the server listen to a given port
server.listen(PORT,HOST, err => {
    if(err) log.error(err);
    else log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});// יוצר שרת שמאזין לפורט מסוים
